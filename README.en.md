# This software is open source software, there is no Google Play, it is not sold anywhere, don't buy it anywhere!
#开发
- Fork of this project at https://github.com/gedoor/MyBookshelf

#软件 screenshot
![image](pictures/1.png)
![image](pictures/2.png)
![image](pictures/3.png)
![image](pictures/4.png)
![image](pictures/5.png)
![image](pictures/6.png)

# Disclaimer
Light reading is a tool to provide online literature search, providing a convenient, fast and comfortable trial reading experience for network literature lovers.

When you search for a book, the book name of the book will be submitted to various third-party online literature websites in the form of keywords.
The content returned by each third-party website is not related to the light reading, and it is not responsible for it, and does not assume any legal responsibility.
Any third-party web pages linked to by using the light-through will be created or provided by others, and you may receive additional services from third-party web pages.
It is not responsible for its legality and does not assume any legal responsibility.
Third-party search engine results are automatically searched for and provided for trial reading based on the title you submitted.
It does not mean that you have read the content or position on a third-party webpage that is approved or searched for.
You should be solely responsible for the results of your use of the search engine.

Lightweight does not guarantee in any way: there is no guarantee that the search results of third-party search engines will meet your requirements.
There is no guarantee that the search service will not be interrupted, and the security, correctness, timeliness and legitimacy of the search results are not guaranteed.
You may not be able to use the light reading properly due to any network conditions, communication lines, third-party websites, etc.
Light reading does not assume any legal responsibility. Lightly respect and protect all personal privacy rights of users who use it.
Personal information such as your registered user name and e-mail address, without your own permission or in accordance with the mandatory provisions of relevant laws and regulations,
Light reading will not be actively disclosed to third parties.

Light reading is committed to minimizing the useless time waste of online literary readers in their own search process.
Showcase the latest chapters of online literature on different websites through professional search.
At the same time, it provides a convenient, fast and comfortable trial reading experience for the majority of novel lovers.
It also enables the rapid and wider dissemination of excellent online literature, thus achieving the goal of promoting the full prosperity and development of online literature to a certain extent.

Light reading encourages the majority of novel lovers to discover excellent online novels and their providers through light reading.
And it is recommended to read the genuine books.
Any third party web content that any organization or individual believes to be linked through a light search may be suspected of infringing its right to disseminate information.
A written notice of written power should be given to the reader in a timely manner, and proof of identity, certificate of ownership and proof of detailed infringement should be provided.
After reading the above legal documents, you will be disconnected from the relevant links as soon as possible.