package com.kunfei.bookshelf.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.MenuItem;

import com.kunfei.basemvplib.impl.IPresenter;
import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.base.BaseTabActivity;
import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.utils.ColorUtil;
import com.kunfei.bookshelf.utils.theme.ThemeStore;
import com.kunfei.bookshelf.view.fragment.SubCategoryFragment;

import java.util.Arrays;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoryListActivity extends BaseTabActivity
{

    public static final String INTENT_CATE_NAME = "name";
    public static final String INTENT_GENDER = "gender";

    private String cate = "";
    private String gender = "";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected List<Fragment> createTabFragments()
    {
        return Arrays.asList(
                SubCategoryFragment.newInstance(cate, "", gender, AppConstant.CateType.NEW),
                SubCategoryFragment.newInstance(cate, "", gender, AppConstant.CateType.HOT),
                SubCategoryFragment.newInstance(cate, "", gender, AppConstant.CateType.REPUTATION),
                SubCategoryFragment.newInstance(cate, "", gender, AppConstant.CateType.OVER));
    }

    public static void startThis(Context context, String name, @AppConstant.Gender String gender)
    {
        Intent intent = new Intent(context, SubCategoryListActivity.class);
        intent.putExtra(INTENT_CATE_NAME, name);
        intent.putExtra(INTENT_GENDER, gender);
        context.startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //设置ToolBar
    private void setupActionBar()
    {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected List<String> createTabTitles()
    {
        return Arrays.asList(getResources().getStringArray(R.array.sub_tabs));
    }

    @Override
    protected IPresenter initInjector()
    {
        return null;
    }

    @Override
    protected void onCreateActivity()
    {
        getWindow().getDecorView().setBackgroundColor(ThemeStore.backgroundColor(this));
        setContentView(R.layout.activity_chapterlist);
        ButterKnife.bind(this);
        setupActionBar();
        mTlIndicator.setSelectedTabIndicatorColor(ThemeStore.accentColor(this));
        mTlIndicator.setTabTextColors(ColorUtil.isColorLight(ThemeStore.primaryColor(this)) ? Color.BLACK : Color.WHITE,
                ThemeStore.accentColor(this));
    }

    @Override
    protected void initData()
    {
        cate = getIntent().getStringExtra(INTENT_CATE_NAME);
        gender = getIntent().getStringExtra(INTENT_GENDER);
    }
}
