package com.kunfei.bookshelf.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.bean.CategoryListBean;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TopCategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context mContext;
    public List<CategoryListBean.MaleBean> list;
    private OnRvItemClickListener mRvItemClickListener;

    public interface OnRvItemClickListener<T>
    {
        void onItemClick(View view, int position, T data);
    }

    public TopCategoryListAdapter(Context context, List<CategoryListBean.MaleBean> list, OnRvItemClickListener listener)
    {
        this.list = list;
        this.mContext = context;
        this.mRvItemClickListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top_category_list,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        CategoryListBean.MaleBean maleBean = list.get(position);
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.mTvName.setText(maleBean.name);
        myViewHolder.mTvBookCount.setText(String.format(mContext.getString(R.string
                .category_book_count), maleBean.bookCount));

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mRvItemClickListener.onItemClick(v, position, maleBean);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView mTvName;
        TextView mTvBookCount;

        MyViewHolder(View itemView)
        {
            super(itemView);
            mTvName = itemView.findViewById(R.id.tvName);
            mTvBookCount = itemView.findViewById(R.id.tvBookCount);
        }
    }
}
