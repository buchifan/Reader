package com.kunfei.bookshelf.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.base.MBaseFragment;
import com.kunfei.bookshelf.bean.SearchBookBean;
import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.presenter.SubCategoryPresenter;
import com.kunfei.bookshelf.presenter.contract.SubCategoryContract;
import com.kunfei.bookshelf.view.activity.SearchBookActivity;
import com.kunfei.bookshelf.view.adapter.SubCategoryAdapter;
import com.kunfei.bookshelf.widget.recycler.refresh.OnLoadMoreListener;
import com.kunfei.bookshelf.widget.recycler.refresh.RefreshRecyclerView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SubCategoryFragment extends
        MBaseFragment<SubCategoryContract.Presenter>
        implements SubCategoryContract.View
{

    public final static String BUNDLE_MAJOR = "major";
    public final static String BUNDLE_MINOR = "minor";
    public final static String BUNDLE_GENDER = "gender";
    public final static String BUNDLE_TYPE = "type";


    private int nStartIndex = 0;
    private int nLimit = 20;
    private View refreshErrorView;
    @BindView(R.id.rfRv_search_books)
    RefreshRecyclerView rfRvSearchBooks;
    private SubCategoryAdapter mSubCategoryAdapter;

    private Unbinder unbinder;
    private ArrayList<SearchBookBean> mSearchBookBeans = new ArrayList<>();

    public static SubCategoryFragment newInstance(String major,
                                                  String minor,
                                                  String gender,
                                                  @AppConstant.CateType String type)
    {
        SubCategoryFragment fragment = new SubCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_MAJOR, major);
        bundle.putString(BUNDLE_GENDER, gender);
        bundle.putString(BUNDLE_MINOR, minor);
        bundle.putString(BUNDLE_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    private String major = "";
    private String minor = "";
    private String gender = "";
    private String type = "";

    @Override
    public int createLayoutId()
    {
        return R.layout.fragment_subcategory;
    }

    @Override
    protected SubCategoryContract.Presenter initInjector()
    {
        return new SubCategoryPresenter();
    }

    @Override
    protected void initData()
    {
        major = getArguments().getString(BUNDLE_MAJOR);
        gender = getArguments().getString(BUNDLE_GENDER);
        minor = getArguments().getString(BUNDLE_MINOR);
        type = getArguments().getString(BUNDLE_TYPE);
        mSubCategoryAdapter = new SubCategoryAdapter(getActivity());
    }

    @Override
    protected void bindView()
    {
        unbinder = ButterKnife.bind(this, view);
        rfRvSearchBooks.setRefreshRecyclerViewAdapter(mSubCategoryAdapter,
                new LinearLayoutManager(getActivity()));
        refreshErrorView = LayoutInflater.from(getActivity())
                .inflate(R.layout.view_refresh_error, null);
        refreshErrorView.findViewById(R.id.tv_refresh_again)
                .setOnClickListener(v -> {

                });
        rfRvSearchBooks.setNoDataAndRefreshErrorView(
                LayoutInflater.from(getActivity())
                        .inflate(R.layout.view_refresh_no_data, null),
                refreshErrorView);

        mSubCategoryAdapter.setItemClickListener((view, position) -> {
            SearchBookActivity.startByKey(getActivity(),
                    mSearchBookBeans.get(position).getName());
        });
    }

    @Override
    protected void bindEvent()
    {
        rfRvSearchBooks.setLoadMoreListener(new OnLoadMoreListener()
        {
            @Override
            public void startLoadMore()
            {
                mPresenter.getBookList(
                        gender,
                        type,
                        major,
                        minor,
                        nStartIndex,
                        nLimit);
            }

            @Override
            public void loadMoreErrorTryAgain()
            {
            }
        });
    }

    @Override
    protected void firstRequest()
    {
        mPresenter.getBookList(gender, this.type, major, minor, 0, nLimit);
    }

    @Override
    public void showBookList(List<SearchBookBean> searchBookBeans)
    {
        if (searchBookBeans.size() == 0)
        {
            rfRvSearchBooks.finishLoadMore(true, true);
            rfRvSearchBooks.finishRefresh(true, true);
            return;
        }
        rfRvSearchBooks.finishRefresh(false, true);
        rfRvSearchBooks.finishLoadMore(false, true);
        mSearchBookBeans.addAll(searchBookBeans);
        nStartIndex = searchBookBeans.size() + nStartIndex;
        mSubCategoryAdapter.upData(SubCategoryAdapter.DataAction.ADD, mSearchBookBeans);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }
}
