package com.kunfei.bookshelf.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kunfei.bookshelf.R;

import java.util.List;

public class AutoCompleteAdapter extends BaseAdapter
{

    private Context mContext;
    private List<String> autoWordsTips;
    private LayoutInflater mInflater;

    public AutoCompleteAdapter(Context context, List<String> autoWordsTip)
    {
        this.mContext = context;
        this.autoWordsTips = autoWordsTip;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount()
    {
        return autoWordsTips.size();
    }

    @Override
    public String getItem(int i)
    {
        return autoWordsTips.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup group)
    {
        ViewHolder mHolder;
        if (convertView == null)
        {
            mHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.item_auto_complete_list, null, true);
            mHolder.tvAutoCompleteItem = (TextView) convertView.findViewById(R.id.tvAutoCompleteItem);
            convertView.setTag(mHolder);
        } else
        {
            mHolder = (ViewHolder) convertView.getTag();
        }
        mHolder.tvAutoCompleteItem.setText(autoWordsTips.get(i));
        return convertView;
    }

    class ViewHolder
    {
        TextView tvAutoCompleteItem;
    }
}
