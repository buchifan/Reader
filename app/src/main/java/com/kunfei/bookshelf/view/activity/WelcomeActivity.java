//Copyright (c) 2017. 章钦豪. All rights reserved.
package com.kunfei.bookshelf.view.activity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kunfei.bookshelf.DbHelper;
import com.kunfei.bookshelf.MApplication;
import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.base.BaseModelImpl;
import com.kunfei.bookshelf.base.observer.MyObserver;
import com.kunfei.bookshelf.bean.BookSourceBean;
import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.model.BookSourceManager;
import com.kunfei.bookshelf.model.analyzeRule.AnalyzeHeaders;
import com.kunfei.bookshelf.model.impl.IHttpGetApi;
import com.kunfei.bookshelf.presenter.ReadBookPresenter;
import com.kunfei.bookshelf.utils.RxUtils;
import com.smarx.notchlib.NotchScreenManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import retrofit2.Response;

public class WelcomeActivity extends AppCompatActivity
{

    public final SharedPreferences preferences = MApplication.getConfigPreferences();

    @BindView(R.id.iv_bg)
    ImageView ivBg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // 避免从桌面启动程序后，会重新实例化入口类的activity
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0)
        {
            finish();
            return;
        }
        setContentView(R.layout.activity_welcome);
        NotchScreenManager.getInstance().setDisplayInNotch(WelcomeActivity.this);

        AsyncTask.execute(DbHelper::getDaoSession);
        ButterKnife.bind(this);
        ValueAnimator welAnimator = ValueAnimator.ofFloat(1f, 0f).setDuration(1000);
        welAnimator.setStartDelay(500);
        welAnimator.addUpdateListener(animation -> {
            float alpha = (Float) animation.getAnimatedValue();
            ivBg.setAlpha(alpha);
        });
        welAnimator.addListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {
                if (preferences.getBoolean(getString(R.string.pk_default_read), false))
                {
                    startReadActivity();
                } else
                {
                    startBookshelfActivity();
                }
                finish();
            }

            @Override
            public void onAnimationEnd(Animator animation)
            {

            }

            @Override
            public void onAnimationCancel(Animator animation)
            {

            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {

            }
        });
        welAnimator.start();
        initData();
    }

    private void startBookshelfActivity()
    {
        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void startReadActivity()
    {
        Intent intent = new Intent(this, ReadBookActivity.class);
        intent.putExtra("openFrom", ReadBookPresenter.OPEN_FROM_APP);
        startActivity(intent);
    }

    public void initData()
    {
        try
        {
            if (!preferences.getBoolean("importDefaultBookSource", false))
            {

                InputStream inputStream = getAssets().open("bookSource.json");
                InputStreamReader inputStreamReader = null;
                try
                {
                    inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                } catch (UnsupportedEncodingException e1)
                {
                    e1.printStackTrace();
                }
                BufferedReader reader = new BufferedReader(inputStreamReader);
                StringBuffer sb = new StringBuffer("");
                String line;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line);
                }
                Observable<List<BookSourceBean>> observable = BookSourceManager.importSource(sb.toString());
                if (observable != null)
                {
                    observable.subscribe(new MyObserver<List<BookSourceBean>>()
                    {
                        @SuppressLint("DefaultLocale")
                        @Override
                        public void onNext(List<BookSourceBean> bookSourceBeans)
                        {
                            preferences.edit()
                                    .putBoolean("importDefaultBookSource",
                                            bookSourceBeans.size() > 0 ? true : false)
                                    .apply();
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            preferences.edit()
                                    .putBoolean("importDefaultBookSource", false)
                                    .apply();
                        }
                    });
                }
            }

            BaseModelImpl.getInstance().getRetrofitString("https://gitee.com")
                    .create(IHttpGetApi.class)
                    .get("https://gitee.com/laishoifh/Reader/raw/light_reader/%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6/config.json",
                            AnalyzeHeaders.getMap(null))
                    .compose(RxUtils::toSimpleSingle)
                    .subscribe(new MyObserver<Response<String>>()
                    {
                        @Override
                        public void onNext(Response<String> response)
                        {
                            if (response.isSuccessful())
                            {
                                String result = response.body();
                                JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
                                boolean allowOpenApp = jsonObject.get("allowOpenApp").getAsBoolean();
                                String strGiteeURl = jsonObject.get("gitee").getAsString();
                                String strDownloadURL = jsonObject.get("downloadUrl").getAsString();

                                preferences.edit()
                                        .putBoolean(AppConstant.KEY_ALLOWOPENAPP, allowOpenApp)
                                        .apply();

                                preferences.edit()
                                        .putString(AppConstant.KEY_GITEE_URL, strGiteeURl)
                                        .apply();

                                preferences.edit()
                                        .putString(AppConstant.KEY_APK_DOWNLOADURL, strDownloadURL)
                                        .apply();
                            }
                        }
                    });
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
