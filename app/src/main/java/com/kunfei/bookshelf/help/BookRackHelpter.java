package com.kunfei.bookshelf.help;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kunfei.bookshelf.base.BaseModelImpl;
import com.kunfei.bookshelf.bean.CategoryListBean;
import com.kunfei.bookshelf.bean.SearchBookBean;
import com.kunfei.bookshelf.model.analyzeRule.AnalyzeHeaders;
import com.kunfei.bookshelf.model.impl.IHttpGetApi;
import com.kunfei.bookshelf.utils.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import io.reactivex.Observable;

public class BookRackHelpter
{

    private static BookRackHelpter mHelpter;

    public static BookRackHelpter getInstance()
    {
        if (mHelpter == null)
        {
            synchronized (HotWordsHelpter.class)
            {
                if (mHelpter == null)
                {
                    mHelpter = new BookRackHelpter();
                }
            }
        }
        return mHelpter;
    }

    public Observable<CategoryListBean> getCategoryList()
    {
        return BaseModelImpl.getInstance().getRetrofitString("http://api.zhuishushenqi.com")
                .create(IHttpGetApi.class)
                .get("http://api.zhuishushenqi.com/cats/lv2/statistics",
                        AnalyzeHeaders.getMap(null))
                .flatMap(response -> analyzeLastReleaseApi(response.body()));
    }

    public Observable<CategoryListBean> analyzeLastReleaseApi(final String jsonStr)
    {
        return Observable.create(e -> {
            CategoryListBean categoryListBean = new CategoryListBean();
            categoryListBean.female = new ArrayList<>();
            categoryListBean.male = new ArrayList<>();
            JsonObject jsonObject = new JsonParser().parse(jsonStr).getAsJsonObject();
            if (jsonObject.has("ok") && jsonObject.get("ok").getAsBoolean())
            {
                if (jsonObject.has("male"))
                {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("male");
                    for (int i = 0; i < jsonArray.size(); i++)
                    {
                        CategoryListBean.MaleBean maleBean = new CategoryListBean.MaleBean(
                                jsonArray.get(i).getAsJsonObject().get("name").getAsString(),
                                jsonArray.get(i).getAsJsonObject().get("bookCount").getAsInt());
                        categoryListBean.male.add(maleBean);
                    }
                }

                if (jsonObject.has("female"))
                {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("female");
                    for (int i = 0; i < jsonArray.size(); i++)
                    {
                        CategoryListBean.MaleBean maleBean = new CategoryListBean.MaleBean(
                                jsonArray.get(i).getAsJsonObject().get("name").getAsString(),
                                jsonArray.get(i).getAsJsonObject().get("bookCount").getAsInt());
                        categoryListBean.female.add(maleBean);
                    }
                }
            }
            e.onNext(categoryListBean);
            e.onComplete();
        });
    }

    public Observable<List<SearchBookBean>> getBooksByCats(String strGender,
                                                           String strType,
                                                           String strMajor,
                                                           String strMinor,
                                                           int nStart,
                                                           int nLimits)
    {
        String url = String.format("http://api.zhuishushenqi.com/book/by-categories?" +
                        "gender=%s&type=%s&major=%s&minor=%s&start=%d&limit=%d",
                strGender, strType, strMajor, strMinor, nStart, nLimits);
        return BaseModelImpl.getInstance().getRetrofitString("http://api.zhuishushenqi.com")
                .create(IHttpGetApi.class)
                .get(url, AnalyzeHeaders.getMap(null))
                .flatMap(response -> analyzeBooksCatsBean(response.body()));
    }

    public Observable<List<SearchBookBean>> analyzeBooksCatsBean(String jsonStr)
    {
        return Observable.create(e -> {
            final String baseCoverUrl = "http://statics.zhuishushenqi.com%s";
            final String baseNoteChapterUrl = "http://api.zhuishushenqi.com/book/%s";
            List<SearchBookBean> searchBookBeans = new ArrayList<>();
            JsonObject jsonObject = new JsonParser().parse(jsonStr).getAsJsonObject();
            if (jsonObject.has("ok") && jsonObject.get("ok").getAsBoolean())
            {
                if (jsonObject.has("books"))
                {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("books");
                    for (int i = 0; i < jsonArray.size(); i++)
                    {

                        JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
                        SearchBookBean searchBookBean = new SearchBookBean();
                        searchBookBean.setName(jsonObj.get("title").getAsString());
                        searchBookBean.setAuthor(jsonObj.get("author").getAsString());
                        searchBookBean.setNoteUrl(String.format(baseNoteChapterUrl,
                                jsonObj.get("_id").getAsString()));
                        searchBookBean.setLastChapter(jsonObj.get("lastChapter").getAsString());
                        searchBookBean.setCoverUrl(String.format(baseCoverUrl,
                                jsonObj.get("cover").getAsString()));
                        searchBookBean.setIntroduce(jsonObj.get("shortIntro").getAsString());
                        searchBookBean.setAddTime(System.currentTimeMillis());

                        LinkedHashSet<String> tagList = new LinkedHashSet<>();
                        tagList.add(jsonObj.get("minorCate").getAsString());
                        JsonArray tagArray = jsonObj.getAsJsonArray("tags");
                        if (tagArray.size() > 0)
                        {
                            for (int tagI = 0; tagI < tagArray.size(); tagI++)
                            {
                                String strKind = tagArray.get(tagI).getAsString();
                                tagList.add(strKind);
                            }
                        }
                        searchBookBean.setKind(StringUtils.join(",", tagList));
                        searchBookBeans.add(searchBookBean);
                    }
                }
            }
            e.onNext(searchBookBeans);
            e.onComplete();
        });
    }
}
