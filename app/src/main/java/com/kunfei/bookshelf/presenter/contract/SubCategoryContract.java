package com.kunfei.bookshelf.presenter.contract;

import com.kunfei.basemvplib.impl.IPresenter;
import com.kunfei.basemvplib.impl.IView;
import com.kunfei.bookshelf.bean.SearchBookBean;

import java.util.List;

public interface SubCategoryContract
{
    public interface Presenter extends IPresenter
    {
        void getBookList(String strGender, String strType, String strMajor, String strMinor, int nStart, int nLimits);
    }

    public interface View extends IView
    {
        void showBookList(List<SearchBookBean> data);
    }
}
